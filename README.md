## Doing exercise about Ruby and Ruby on Rails

**Ruby :**  
[Introduction to Programming with Ruby](https://launchschool.com/books/ruby)  

**Ruby OOP :**  
[Object Oriented Programming with Ruby](https://launchschool.com/books/oo_ruby)  
 
**Ruby on Rails :**  
[Getting Started with Rails](https://guides.rubyonrails.org/getting_started.html)  
[Getting Started (Rails) - MongoDB](https://docs.mongodb.com/mongoid/current/tutorials/getting-started-rails/)  
[Build a Rails 5 App with MongoDB](https://github.com/bountonw/rails5-mongoid)

**Ruby on Rails API :**  
[Build a RESTful JSON API With Rails 5 - Part One](https://www.digitalocean.com/community/tutorials/build-a-restful-json-api-with-rails-5-part-one)  
[Build a RESTful JSON API With Rails 5 - Part Two](https://scotch.io/tutorials/build-a-restful-json-api-with-rails-5-part-two)  
[Build a RESTful JSON API With Rails 5 - Part Three](https://scotch.io/tutorials/build-a-restful-json-api-with-rails-5-part-three)  
[Setting up an API only Rails application using MongoDB (with Mongoid)](https://milleringtonsmythe.wordpress.com/2017/06/03/setting-up-an-api-only-rails-application-using-mongodb-with-mongoid/)
