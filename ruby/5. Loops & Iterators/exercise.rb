#No.1
x = [1, 2, 3, 4, 5]
x.each do |a|
  a + 1
end
#setelah semua baris dijalankan tidak merubah apa-apa, array pada x tetap bernilai [1, 2, 3, 4, 5]

#No.2
input = ""
while input != "STOP" do
  puts "Hi, How are you feeling?"
  answer = gets.chomp
  puts "Want me to ask you again?"
  input = gets.chomp
end

#No.3
def count_down_to_zero(number)
  if number <= 0
    puts number
  else
    puts number
    count_down_to_zero(number-1)
  end
end

count_down_to_zero(21)