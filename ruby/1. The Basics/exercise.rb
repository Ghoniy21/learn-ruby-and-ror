#No.1
first_name = "Abdul"
last_name = "Ghoniy"

puts "#{first_name} #{last_name}"

#No.2
number = 2021
thousand_place = number / 1000
hundred_place = number % 1000 / 100
tens_place = number % 100 /10
ones_place = number % 10

puts "thousand: #{thousand_place}, hundred: #{hundred_place}, tens: #{tens_place}, ones: #{ones_place}"

#No.3
movies = {
    :the_raid => 2012,
    :an_honest_liar => 2014,
    :blood_punch => 2014,
    :detour => 1945,
    :the_negotiation => 2018,
    :black_widow => 2021
}

movies.each{ |key, val|
    puts val
}

#No. 4
movies_year = [2012, 2014, 2014, 1945, 2018, 2021]
i = 0

while i < movies_year.length()
    puts movies_year[i]
    i += 1
end

#No. 5
positive_number = 5
result = 1

while positive_number > 0
    result *= positive_number
    positive_number -= 1
end

puts result

#No. 6
numbers = [22.7, 19.97, 21.4]
i = 0

while i < numbers.length()
    puts numbers[i] * numbers[i]
    i += 1
end

#No. 7
#SyntaxError: (irb):2: syntax error, unexpected ')', expecting '}'
#from /usr/local/rvm/rubies/ruby-2.5.3/bin/irb:16:in `<main>'
#
#Memiliki arti bahwa terdapat kurawal buka "{" yang belum diakhiri dengan kurawal tutup "}"