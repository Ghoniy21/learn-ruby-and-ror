#No.1
puts "What is your name?"
name = gets.chomp
puts "Hello "+name

#No.3
10.times do
    puts name
end

#No.4
puts "What is your first name?"
first_name = gets.chomp
puts "What is your last name?"
last_name = gets.chomp
puts "Your full name is "+first_name+" "+last_name

#No.5
#Program pertama menghasilkan output 3
#Sedangkan program kedua menghasilkan error karena variabel x berada dalam scope do end