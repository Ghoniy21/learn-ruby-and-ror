#No.1
def check_lab_in(word)
    if /lab/.match(word)
        word
    else
        "No match here"
    end
end

puts check_lab_in("laboratory")
puts check_lab_in("experiment")
puts check_lab_in("Pans Labyrinth")
puts check_lab_in("elaborate")
puts check_lab_in("polar bear")

#No.2
def execute(&block)
    block
end

execute { puts "Hello from inside the execute method!" }
#tidak mereturn apapun karena block tidak melakukan call

#No.3
#Exception handling digunakan untuk menanganni kemungkinan error pada program tanpa harus menghentikan program

#No.4
def execute(&block)
    block.call
end

execute { puts "Hello from inside the execute method!" }

#No.5
def execute(block)
    block.call
end

execute { puts "Hello from inside the execute method!" }
#Menghasilkan error karena parameter block pada method execute tidak diawali simbol &