arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
#No.1
arr.each{ |number|
    puts number
}

#No.2
arr.each{ |number|
    if number > 5
        puts number
    end
}

#No.3
new_arr = arr.select { |number| number.odd?}

puts new_arr

#No.4
arr.push(11)
arr.unshift(0)

#No.5
arr.pop
arr.push(3)

#No.6
puts "#{arr.uniq}"

#No.7
#Hash menggunakan key => value, sedangkan array menggunakan indexing

#No.8
hash = {:name => 'bob'}
hash = {name: 'bob'}
#Baris 34 menggunakan cara penulisan lama, sedangkan baris 35 menggunakan cara penulisan baru
#Baris 34 dan baris 35 menghasilkan output yang sama

#No.9
h = {a:1, b:2, c:3, d:4}

puts h[:b]
h[:e] = 5
h.delete_if { |key, val|
    val < 3.5
}

#No.10
#Hash dapat berisi array, array pun dapat berisi hash
#berikut contohnya:
# hash berisi array
hash = {names: ['bob', 'joe', 'susan']}

# array berisi hash
arr = [{name: 'bob'}, {name: 'joe'}, {name: 'susan'}]

#No.11
contact_data = [["joe@email.com", "123 Main st.", "555-123-4567"],
            ["sally@email.com", "404 Not Found Dr.", "123-234-3454"]]

contacts = {"Joe Smith" => {}, "Sally Johnson" => {}}

contacts["Joe Smith"][:email] = contact_data[0][0]
contacts["Joe Smith"][:address] = contact_data[0][1]
contacts["Joe Smith"][:phone] = contact_data[0][2]
contacts["Sally Johnson"][:email] = contact_data[1][0]
contacts["Sally Johnson"][:address] = contact_data[1][1]
contacts["Sally Johnson"][:phone] = contact_data[1][2]

puts "#{contacts}"

#No.12
puts "Joe's email is: #{contacts["Joe Smith"][:email]}"
puts "Sally's phone number is: #{contacts["Sally Johnson"][:phone]}"

#No.13
arr = ['snow', 'winter', 'ice', 'slippery', 'salted roads', 'white trees']

puts "#{arr.delete_if { |str| str.start_with?("s") }}"

puts "#{arr.delete_if { |str| str.start_with?("s", "w") }}"

#No.14
a = ['white snow', 'winter wonderland', 'melting ice',
    'slippery sidewalk', 'salted roads', 'white trees']

b = a.map { |pairs| pairs.split }
b = b.flatten
puts "#{b}"

#No.15
hash1 = {shoes: "nike", "hat" => "adidas", :hoodie => true}
hash2 = {"hat" => "adidas", :shoes => "nike", hoodie: true}

if hash1 == hash2
  puts "These hashes are the same!"
else
  puts "These hashes are not the same!"
end
#Menghasilkan "These hashes are the same!" karena tiap value pada hash1 sama dengan tiap value pada hash2

#No.16
fields = [:email, :address, :phone]

new_contact = contacts.each_with_index do |(name, hash), index|
                fields.each do |field|
                    hash[field] = contact_data[index].shift
                end
            end

puts "#{new_contact}"