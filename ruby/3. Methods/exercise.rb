#No. 1
def greeting(name)
  return "Hello " + name
end

puts greeting("Oni")

#No. 2
x = 2
#inisialisasi variabel x dengan nilai 2
puts x = 2
#inisialisasi kembali nilai variabel x lalu ditampilkan nilai x, variabel x ini kemudian bernilai nil
p name = "Joe"
#inisialisasi variabel name dengan nilai joe lalu ditampilkan nilai dari variabel name, variabel name tetap bernilai "Joe"
four = "four"
#inisialisasi variabel four dengan nilai "four"
print something = "nothing"
#inisialisasi variabel something dengan nilai "nothing" lalu ditampilkan nilai dari variabel something, variabel something kemudian bernilai nil
#perintah print tidak membuat baris baru

#No. 3
def multiply(num1, num2)
  return num1 * num2
end

puts multiply(2, 1)

#No. 4
def scream(words)
  words = words + "!!!!"
  return
  puts words
end

scream("Yippeee")
#tidak menghasilkan output apa-apa karena method scream tidak mereturn sesuatu dan baris dibawahnya tidak dieksekusi

#No. 5
def scream(words)
  words = words + "!!!!"
  puts words
end

scream("Yippeee")
#hapus perintah return sehingga menampilkan nilai dari words

#No. 5
#error dikarenakan method calculate_product memiliki 2 parameter, sedangkan program hanya memasukkan 1 parameter ketika memanggilnya