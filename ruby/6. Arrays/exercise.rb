#No.1
arr = [1, 3, 5, 7, 9, 11]
number = 3

if arr.include?(number)
    puts "#{number} is in the array."
end

#No.2
arr = ["b", "a"]
arr = arr.product(Array(1..3))
#arr = [["b", 1], ["b", 2], ["b", 3], ["a", 1], ["a", 2], ["a", 3]]
arr.first.delete(arr.first.last)
#Pada program ini setelah dijalankan, arr akan bernilai seperti pada baris 12
#Baris 13 mereturn nilai 1, didapat dari last pada index pertama yaitu ["b", 1]

arr = ["b", "a"]
arr = arr.product([Array(1..3)])
#arr = [["b", [1, 2, 3]], ["a", [1, 2, 3]]]
arr.first.delete(arr.first.last)
#Pada program ini setelah dijalankan, arr akan bernilai seperti pada baris 19
#Baris 20 mereturn nilai [1, 2, 3], didapat dari last pada index pertama yaitu ["b", [1, 2, 3]]

#No.3
arr = [["test", "hello", "world"],["example", "mem"]]
puts arr.last.first

#No.4
arr = [15, 7, 18, 5, 12, 8, 5, 1]

arr.index(5)
#mereturn nilai index dari array dengan nilai 5 pada arr, yaitu index ke 3
#arr.index[5]
#error karena penggunaan method seharusnya menggunakan kurung ()
arr[5]
#mereturn nilai index ke-5 pada arr yaitu 8

#No.5
string = "Welcome to America!"
a = string[6]
b = string[11]
c = string[19]
#variabel a bernilai 'e'
#variabel b bernilai 'A'
#variabel c bernilai nil, karena index pada variabel string hanya sampai 18

#No.6
names = ['bob', 'joe', 'susan', 'margaret']
names[3] = 'jody'

puts "#{names}"

#No.7
names.each_with_index{ |name, index|
    puts "Index #{index} memiliki nilai #{name}"
}

#No.8
numbers = [1, 9, 9, 7]

new_numbers = numbers.map{ |number|
    number + 2
}

p numbers
p new_numbers