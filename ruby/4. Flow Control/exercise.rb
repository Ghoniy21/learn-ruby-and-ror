#No.1
(32 * 4) >= 129
#return false karena 128 < 129
false != !true
#return false karena false != false
true == 4
#return false karena 4 bertipe number
false == (847 == '874')
#return true karena false == false
(!true || (!(100 / 5) == 20) || ((328 / 4) == 82)) || false
#return true karena terdapat 1 nilai true dalam (false || false || true) || false

#No.2
def all_caps(string)
  if string.length > 10
    return string.upcase!
  else
    return string
  end
end

puts all_caps("Hello World")

#No. 3
puts "Input number (0-100):"
number = gets.chomp.to_i

if number < 0
  puts "Input positive number"
elsif number <= 50
  puts "#{number} is between 0 and 50"
elsif number <= 100
  puts "#{number} is between 51 and 100"
else
  puts "#{number} is above 100"
end

#No.4
# Snippet 1
'4' == 4 ? puts("TRUE") : puts("FALSE")
#Snippet 1 mencetak FALSE karena '4' == 4 bernilai false (string tidak sama dengan number)

# Snippet 2
x = 2
if ((x * 3) / 2) == (4 + 4 - x - 3)
  puts "Did you get it right?"
else
  puts "Did you?"
end
#Snippet 2 mencetak "Did you get it right?" karena 3 == 3 bernilai true

# Snippet 3
y = 9
x = 10
if (x + 1) <= (y)
  puts "Alright."
elsif (x + 1) >= (y)
  puts "Alright now!"
elsif (y + 1) == x
  puts "ALRIGHT NOW!"
else
  puts "Alrighty!"
end
#Snippet 3 mencetak karena "Alright now!" karena 11 >= 9 (baris 57) dan perintah selanjutnya tidak dieksekusi

#No.5
def equal_to_four(x)
  if x == 4
    puts "yup"
  else
    puts "nope"
  end   #Kita perlu menambahkan end dibaris ini untuk menutup scope pada if
end

equal_to_four(5)

#No.6
(32 * 4) >= "129"
#Menghasilkan error karena string tidak dapat dicompare dengan number
847 == '874'
#Menghasilkan false karena string tidak sama dengan number
'847' < '846'
#Menghassilkan false karena 7 > 6
'847' > '846'
#Menghasilkan true karena 7 > 6
'847' > '8478'
#Menghasilkan false karena panjang string '847' lebih pendek dari string '8478', sehingga ketika membandingkan '8' pada '8478' dengan '847' menghasilkan < supaya true
'847' < '8478'
#Menghasilkan true seperti pada penjelasan baris 87