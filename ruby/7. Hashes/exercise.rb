#No.1
family = {  uncles: ["bob", "joe", "steve"],
            sisters: ["jane", "jill", "beth"],
            brothers: ["frank","rob","david"],
            aunts: ["mary","sally","susan"]
        }

immediate_family = family.select{ |key, val|
    key==:sisters || key==:brothers
}

p immediate_family.values.flatten

#No.2
person = {name: "Bob"}
age = {age: "70 Kg"}
person.merge(age)
puts person
person.merge!(age)
puts person
#Perbedaan pada merge dan merge! adalah merge tidak mengubah nilai hash original dan mereturn hash baru
#Sedangkan merge! mengubah nilai hash original

#No.3
family.each_key{ |key|
    puts key
}

family.each_value{ |val|
    puts val
}

family.each{ |key, val|
    puts "The value of key #{key} is #{val} "
}

#No.4
person = {name: 'Bob', occupation: 'web developer', hobbies: 'painting'}

puts person[:name]

#No.5
#Menggunakan method "value?"
puts "Contains a specific value" if person.value?("Bob")

#No.6
x = "hi there"
my_hash = {x: "some value"}
# X disini menjadi key dalam bentuk simbol
my_hash2 = {x => "some value"}
# X disini menjadi key dalam bentuk string

#No.7
#NoMethodError: undefined method `keys' for Array
#
#Memiliki arti bahwa Array tidak memiliki method bernama 'keys'