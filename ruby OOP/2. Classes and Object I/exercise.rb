#No.1
class MyCar
    def initialize(year, color, model)
        @year = year
        @color = color
        @model = model
        @current_speed = 0
    end

    def speed_up(speed)
        @current_speed += speed
    end

    def brake(speed)
        @current_speed -= speed
    end

    def car_off
        @current_speed = 0
    end

    def speed_info
        puts "You are now going #{@current_speed} mph."
    end
end

impreza = MyCar.new(2021, "red", "subaru impreza")

impreza.speed_info
impreza.speed_up(40)
impreza.speed_info
impreza.speed_up(50)
impreza.brake(30)
impreza.speed_info

#No.2
class MyCar2
    attr_accessor :color
    attr_reader :year

    def initialize(year, color, model)
        @year = year
        @color = color
        @model = model
        @current_speed = 0
    end

    def color=(new_color)
        @color = new_color
    end

    def color
        @color
    end

    def year
        @year
    end
end

impreza2 = MyCar2.new(2021, "red", "subaru impreza")

puts impreza2.color
impreza2.color = "white"
puts impreza2.color
puts impreza2.year

#No.3
class MyCar3
    attr_accessor :color
    attr_reader :year

    def initialize(year, color, model)
        @year = year
        @color = color
        @model = model
        @current_speed = 0
    end

    def spray_paint(new_color)
        self.color = new_color
        puts "Your cat has been successfully painted into #{new_color} color."
    end
end

impreza3 = MyCar3.new(2021, "red", "subaru impreza")

puts impreza3.color
impreza3.spray_paint("white")
puts impreza3.color