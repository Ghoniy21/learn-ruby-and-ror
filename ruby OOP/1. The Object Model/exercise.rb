#No.1
class GoodDog
end

sparky = GoodDog.new
#Untuk membuat Object kita memerlukan sebuah class terlebih dahulu, lalu melakukan instansiasi dari kelas tersebut dengan menggunakan kata kunci "new"

#No.2
module Speak
    def speak(sound)
        puts "#{sound}"
    end
end

class GoodDog
    include Speak
end
#Module merupakan kumpulan fungsi yang dapat digunakan kembali pada kelas lain
#Agar dapat menggunakan Module pada sebuah Class kita memanggilnya dengan menggunakan perintah "include"