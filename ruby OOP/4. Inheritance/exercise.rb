#No.1, No.2, No.3, No.5 & No.6
module Towable
    def can_tow?(pounds)
        pounds < 2000 ? true : false
    end
end

class Vehicle
    attr_accessor :color
    attr_reader :model, :year
    @@number_of_vehicles = 0

    def initialize(year, color, model)
        @year = year
        @model = model
        @color = color
        @current_speed = 0
        @@number_of_vehicles += 1
    end

    def speed_up(speed)
        @current_speed += speed
    end

    def brake(speed)
        @current_speed -= speed
    end

    def car_off
        @current_speed = 0
    end

    def speed_info
        puts "You are now going #{@current_speed} mph."
    end

    def spray_paint(new_color)
        self.color = new_color
        puts "Your cat has been successfully painted into #{new_color} color."
    end

    def self.number_of_vehicles
        puts "The number of vehicles is #{@@number_of_vehicles}"
    end

    def self.gas_mileage(gallons, miles)
        puts "#{miles / gallons} miles per gallon of gas"
    end

    def age
        "Your #{self.model} is #{years_old} years old."
    end

    private

    def years_old
        Time.now.year - self.year
    end
end

class MyCar < Vehicle
    NUMBER_OF_DOORS = 4

    def to_s
        "My car attribute is #{self.color}, #{self.year}, #{self.model}!"
    end
end

class MyTruck < Vehicle
    include Towable

    NUMBER_OF_DOORS = 2

    def to_s
        "My truck attribute is #{self.color}, #{self.year}, #{self.model}!"
    end
end

#No.4
puts "---Vehicle method lookup---"
puts Vehicle.ancestors
puts "---MyCar method lookup---"
puts MyCar.ancestors
puts "---MyTruck method lookup---"
puts MyTruck.ancestors

#No.5
impreza = MyCar.new(2021, "red", "subaru impreza")

impreza.speed_info
impreza.speed_up(40)
impreza.speed_info
impreza.speed_up(50)
impreza.brake(30)
impreza.speed_info
impreza.car_off
MyCar.gas_mileage(13, 351)
impreza.spray_paint("white")
puts impreza
puts impreza.age

#No.7
class Student
    def initialize(name, grade)
        @name = name
        @grade = grade
    end

    def better_grade_than?(other_student)
        grade > other_student.grade
    end

    protected

    def grade
        @grade
    end
end

joe = Student.new("Joe", 90)
bob = Student.new("Bob", 84)
puts "Well done!" if joe.better_grade_than?(bob) 

#No.8
#Program error dikarenakan method hi merupakan private method, sehingga hanya method tersebut hanya dapat dipanggil didalam kelasnya saja