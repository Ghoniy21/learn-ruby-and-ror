#No.1
class MyCar
  def self.gas_mileage(gallons, miles)
    puts "#{miles / gallons} miles per gallon of gas"
  end
end

MyCar.gas_mileage(13, 351)

#No.2
class MyCar2
  attr_accessor :color
  attr_reader :year
  
  def initialize(year, color, model)
      @year = year
      @color = color
      @model = model
      @current_speed = 0
  end

  def to_s
    "My car attribute is a #{color}, #{year}, #{@model}!"
  end
end

impreza2 = MyCar2.new(2021, "red", "subaru impreza")

puts impreza2

#No.3
class Person
  attr_writer :name
  def initialize(name)
    @name = name
  end
end

bob = Person.new("Steve")
bob.name = "Bob"
#attr_reader hanya untuk membaca sebuah nilai, tidak bisa memberikan sebuah nilai
#kebalikannya, attr_writer dapat digunakan untuk memberikan sebuah nilai, namun tidak dapat membaca/mereturn sebuah nilai